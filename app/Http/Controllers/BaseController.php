<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Message;

class BaseController extends Controller
{
  	public function showHome(){
    	return view('layouts.home');
	}
	public function testInput($input){
    	dd($input);
	}
	// ini kalo inputannya ada banyak, lebih dari 1
	// public function testInput($input, Request $request){
    //    dd($request->input());
	//}
	public function storeMessage(Request $request){
        // dd($request->input());
        //yang dibawah ini untuk yang dari inputan ke database
        $messageObj = new Message;
		$messageObj->name = $request->input('name');
		$messageObj->email = $request->input('email');
		$messageObj->phone = $request->input('phone');
		$messageObj->message = $request->input('message');
		$messageObj->save();
	}
	public function showMessage(){
	    $messages = Message::all();
	    return view('layouts.messages', compact('messages'));
	}
}
